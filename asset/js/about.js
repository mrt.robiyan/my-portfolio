
const scriptURL = 'https://script.google.com/macros/s/AKfycbxFBQGrSQmwemG2sw42MJSocPaY8M9wwY-G5_qqPi8e53mNqgl6U4Y0zx5RYoNQEco/exec'
const form = document.forms[ 'iyan-net-contact-form' ];

const btnSent = document.querySelector('.btn-sent');
const btnLoading = document.querySelector('.btn-loading');
const costumAlert = document.querySelector('.costum-alert');

form.addEventListener('submit', e => {
    e.preventDefault();
    btnLoading.classList.toggle('d-none');
    btnSent.classList.toggle('d-none');
    
    fetch(scriptURL, { method: 'POST', body: new FormData(form)})
        .then(response => {
            btnLoading.classList.toggle('d-none');
            btnSent.classList.toggle('d-none');
            costumAlert.classList.toggle('d-none');
            form.reset();
            console.log('Success!', response);
        })
    .catch(error => console.error('Error!', error.message))
})
